# Info
Sparta is a spammer to those who want to have an application which is able to delegate PoW to workers around the world or just inside a private network. Sparta is the brain while other machines are the muscles.

# Building

```
git clone https://gitlab.com/brunoamancio/spartaSpammer.git && cd spartaSpammer && go get -d ./...
go build -o sparta ./main
```

#### Running with configuration in command-line (mixing short with long config entry name)
```
./sparta -m="My transaction message" -p="http://powserver1.com:16000,http://powserver2.com:16000" -e=5 --workers.tipReceivers=5
```
#### Running with configuration file
```
./sparta --config="sparta.json"
```


# Configuration
You can either enter configuration directly on the command line (when executing sparta) or use a configuration file. 



Below are the configuration options:


|**Entry name** |**short**  | **Default value** |**Description**
|--|--|--|--
|logs.level      |l| "INFO" | Minimum level to log
|tips.maxDepth   |d| 3 | Maximum depth to search for tips
|transactions.minWeightMagnitude |w|14| Current minWeightMagnitude of the network
|transactions.recipientAddress|a|Sparta's address | Address which receives spam transactions
|transactions.tag|t| Sparta's tag | Tag of spam transactions
|transactions.message|m| "THIS IS SPARTA!"" | Message of spam transactions
|nodes.addresses|n| Empty | Nodes used to requests tips and to broadcast transactions
|nodes.blackListedAddresses| b | Empty | Nodes which will NOT be used by the spammer (Useful when loading lists of nodes via json)
|workers.tipReceivers|r| 12 | Amount of workers which will request tips from nodes
|workers.powServers|p| Empty | Remote pow servers addresses
|config|c|"sparta.json"|Config file path


# PoW

## Remote PoW

Sparta is fully compatible with [RemotePoW](https://gitlab.com/brunoamancio/remotepow). 
You can set up your servers which will do the PoW calculations with the instructions from that repository.
In that manner, Sparta delegates all the PoW needed to those servers. 
That means, Sparta is lightweight and does not use local resources for that purpose. Scalability is the word here.

## Local PoW

If you have a PiDiver, you can use it to generate the PoW for you. Currently only that form of local PoW is implemented.
In this case, configure workers.powServers to be empty (or localhost):

```
"powServers" : [""]
```



# TODOs
- Add support for delegating tip loading to other machines (using websockets)
- Add support for requesting PoW through web sockets