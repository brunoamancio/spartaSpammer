package tipsreceiver

import (
	"time"

	"../logs"
	"../nodes"
	"../transactionssender"
	"github.com/iotaledger/giota"
	// todo download code and use it locally to be sure
)

var receiverIsClosing = false

var transactionsInLastPeriod = int64(0)
var durationInLastPeriod = int64(0)

func Init(depth int, tipReceiverWorkersCount int) {
	logs.Log.Infof("Initializing tips receiver with %v workers", tipReceiverWorkersCount)

	go reportReceivedPerSecond()

	loadTipReceivingWorkers(depth, tipReceiverWorkersCount)

	go keepWorkersAlive(depth, tipReceiverWorkersCount)

	logs.Log.Info("Tips receiver initialized")
}

var workerCounter = 0

// TODO: Integrate this to getTransactionsToApprove
func keepWorkersAlive(depth, tipReceiverWorkersCount int) {
	periodSeconds := 60
	ticker := time.NewTicker(time.Second * time.Duration(periodSeconds))
	for range ticker.C {

		if workerCounter >= tipReceiverWorkersCount {
			logs.Log.Infof("Workers alive: %v - No need to add fresh workers", workerCounter)
		} else {
			for workerCounter < tipReceiverWorkersCount {
				apiNode, apiNodeAddress := addWorker()
				logs.Log.Infof("Workers alive: %v - Adding new worker to get TTA from '%s' and reach %v workers", workerCounter, apiNodeAddress, tipReceiverWorkersCount)
				go getTransactionsToApprove(apiNode, depth)
			}
		}
	}
}

func addWorker() (*giota.API, string) {
	apiNode := nodes.GetRandomApiNode()
	apiNodeAddress := nodes.GetNodeAddress(apiNode)

	workerCounter++
	return apiNode, apiNodeAddress
}

func loadTipReceivingWorkers(depth, tipReceiverWorkersCount int) {

	for workerCounter < tipReceiverWorkersCount {
		apiNode := nodes.GetRandomApiNode()
		apiNodeAddress := nodes.GetNodeAddress(apiNode)
		logs.Log.Infof("Starting worker to get TTA from '%s'", apiNodeAddress)
		go getTransactionsToApprove(apiNode, depth)

		workerCounter++
	}
}

func getTransactionsToApprove(apiNode *giota.API, depth int) {
	apiNodeAddress := nodes.GetNodeAddress(apiNode)

	logs.Log.Debugf("A worker started loading tips from '%s'", apiNodeAddress)

	var errorsInARowCount = 0
	const acceptableErrorsInARow int = 5
	for !receiverIsClosing {
		transactionsToApproveResponse, err := apiNode.GetTransactionsToApprove(int64(depth), int64(0), "")
		if err != nil {
			errorMessage := err.Error()

			goodNodeResponse, modifiedResponse := nodes.HandleNodeResponse(errorMessage)
			if modifiedResponse != "" {
				errorMessage = modifiedResponse
			}

			if errorsInARowCount < acceptableErrorsInARow || goodNodeResponse {
				logs.Log.Infof("Could not load tips from '%s' - Retrying - Cause: %s", apiNodeAddress, errorMessage)
				errorsInARowCount++
				continue
			}

			// Kill this worker
			workerCounter--
			logs.Log.Infof("Could not load tips from '%s' - Worker was killed - Cause: %s", apiNodeAddress, errorMessage)
			break
		}

		if receiverIsClosing { // Check again!
			break
		}

		transactionsInLastPeriod++
		durationInLastPeriod += transactionsToApproveResponse.Duration

		gTTA := new(transactionssender.GTTAResponse)
		gTTA.Response = transactionsToApproveResponse
		gTTA.Node = apiNodeAddress
		transactionssender.TxsInPreparationChannel <- gTTA

		if transactionsToApproveResponse.Duration > 3500 {
			_, nodesAddress := addWorker()
			logs.Log.Infof("Node '%s' too slow. Replacing it with '%s'.", gTTA.Node, nodesAddress)

			workerCounter--
			break // Kill worker
		}
	}
}

func reportReceivedPerSecond() {
	periodSeconds := 30
	minuteTicker := time.NewTicker(time.Second * (time.Duration)(periodSeconds))
	for range minuteTicker.C {
		tps := float64(transactionsInLastPeriod) / float64(periodSeconds)
		avgDuration := float64(durationInLastPeriod) / float64(periodSeconds)
		logs.Log.Warningf("Last 30 secs: %v - Avg TTA responses/s: %.1f - Avg duration: %.1fms", transactionsInLastPeriod, tps, avgDuration)
		transactionsInLastPeriod = 0
		durationInLastPeriod = 0
	}
}

func Dispose() {
	logs.Log.Debug("Finalizing tips receiver")
	receiverIsClosing = true
	logs.Log.Info("Tips receiver finalized")
}
