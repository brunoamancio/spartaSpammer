package utils

import (
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"

	"../logs"

	"github.com/iotaledger/giota"
)

var rng *rand.Rand
var randLock = &sync.Mutex{}

func init() {
	rng = rand.New(rand.NewSource(time.Now().UnixNano()))
}

func Random(min, max int) int {
	// Need to lock this on smaller devices: https://github.com/golang/go/issues/3611
	randLock.Lock()
	defer randLock.Unlock()
	return rng.Intn(max-min) + min
}

func FromString(s string) giota.Trytes {
	Alphabet := "9ABCDEFGHIJKLMNOPQRSTUVWXYZ"

	var output string
	chars := []rune(s)

	for _, c := range chars {
		v1 := c % 27
		v2 := (c - v1) / 27
		output += string(Alphabet[v1]) + string(Alphabet[v2])
	}
	return giota.Trytes(output)
}

func getJSON(url string, target interface{}) error {
	var myClient = &http.Client{Timeout: 10 * time.Second}

	resp, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(result, target)
	return err
}

func ReadNodeList(addresses []string) []string {
	var allNodeList []string

	for i := 0; i < len(addresses); i++ {
		currentAddress := addresses[i]
		var nodesFromResource []string

		if strings.HasSuffix(currentAddress, ".json") {
			err := getJSON(currentAddress, &nodesFromResource)
			if err != nil {
				logs.Log.Errorf("Could not load nodes from resource '%s'", err)
				continue
			}

			allNodeList = append(allNodeList, nodesFromResource...)
		} else {
			allNodeList = append(allNodeList, currentAddress)
		}
	}

	return allNodeList
}
