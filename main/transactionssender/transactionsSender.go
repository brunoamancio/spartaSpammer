package transactionssender

import (
	"fmt"
	"time"

	"../logs"
	"../nodes"
	"../pow"
	"../utils"
	"github.com/iotaledger/giota"
)

var address giota.Address
var tag giota.Trytes
var message giota.Trytes
var campaign giota.Trytes

type GTTAResponse struct {
	Response *giota.GetTransactionsToApproveResponse
	Node     string
}

var TxsInPreparationChannel chan *GTTAResponse
var transactionsToSendChannel chan []giota.Transaction

func Configure(addressString, tagString, messageString, campaignString string) {
	address = giota.Address(addressString)
	tag = giota.Trytes(tagString)
	message = giota.Trytes(messageString)
	campaign = giota.Trytes(campaignString)

	TxsInPreparationChannel = make(chan *GTTAResponse)
	transactionsToSendChannel = make(chan []giota.Transaction)

	go processTxsInPreparation()
	go processTransactionsToSend()

	go reportSentPerSecond()
}

func processTxsInPreparation() {
	for transactionsToApprove := range TxsInPreparationChannel {
		logs.Log.Debugf("Duration: %vms | Got TTA from '%s'", transactionsToApprove.Response.Duration, transactionsToApprove.Node)

		bundle := generateBundle(transactionsToApprove)

		txPendingPow := new(pow.TxPendingPow)
		txPendingPow.Bundle = bundle
		txPendingPow.TransactionToApprove = transactionsToApprove.Response
		txPendingPow.TxPowCalculatedResponseChannel = transactionsToSendChannel

		pow.TxsPendingPowChannel <- txPendingPow
	}
}

func processTransactionsToSend() {
	for transactionToSend := range transactionsToSendChannel {

		go requestBroadcastTransaction(transactionToSend)

		if senderIsClosing {
			isLastTransaction := len(transactionsToSendChannel) <= 1 && len(pow.TxsPendingPowChannel) <= 1 && len(TxsInPreparationChannel) <= 1
			if isLastTransaction {
				pow.Dispose()
			}
		}
	}
}

func generateBundle(gTTA *GTTAResponse) giota.Bundle {
	stringMessage := fmt.Sprintf("{ \"Greeting\": \"%s\", \"Node\": \"%s\", \"Duration\": %v, \"Campaign\" : \"%s\"}", message, gTTA.Node, gTTA.Response.Duration, campaign)

	transfers := []giota.Transfer{
		giota.Transfer{
			Address: address,
			Value:   0,
			Tag:     tag,
			Message: giota.Trytes(utils.FromString(stringMessage)),
		},
	}

	bundle, err := giota.PrepareTransfers(nil, "", transfers, nil, "", 2)
	if err != nil {
		logs.Log.Fatalf("PrepareTransfers: %v", err.Error())
	}

	logs.Log.Debug("Bundle generated")
	return bundle
}

func requestBroadcastTransaction(transactionsWithPow []giota.Transaction) {
	logs.Log.Debug("Starting to broadcast a transaction")

	const maxTries = 8

	for i := 1; i < maxTries+1; i++ {
		apiNode := nodes.GetRandomApiNode()

		err := apiNode.BroadcastTransactions(transactionsWithPow)
		if err != nil {
			errorMessage := err.Error()

			goodNodeResponse, modifiedResponse := nodes.HandleNodeResponse(errorMessage)
			if modifiedResponse != "" {
				errorMessage = modifiedResponse
			}
			apiNodeAddress := nodes.GetNodeAddress(apiNode)
			logs.Log.Errorf("Try (%v) - Could not broadcast transaction to '%s'. Cause: %s", i, apiNodeAddress, errorMessage)

			if goodNodeResponse {
				break // Request failed but it is not critical error
			}

			continue // Continue trying with other nodes
		}

		logs.Log.Debug("Transaction broadcasted")
		sentInLastPeriod++
		break
	}
}

var sentInLastPeriod = 0

func reportSentPerSecond() {
	periodSeconds := 30
	minuteTicker := time.NewTicker(time.Second * (time.Duration)(periodSeconds))
	for range minuteTicker.C {
		tps := float64(sentInLastPeriod) / float64(periodSeconds)
		logs.Log.Warningf("Last 30 secs sent TTA responses: %v - Average/s: %.1f", sentInLastPeriod, tps)
		sentInLastPeriod = 0
	}
}

var senderIsClosing = false

func Dispose() {
	logs.Log.Debug("Finalizing transactions sender")
	senderIsClosing = true
	logs.Log.Info("Transactions sender finalized")
}
