package main

import (
	"sync"

	"./initializer"
)

var waitGroup = &sync.WaitGroup{}

func main() {
	waitGroup.Add(1) // Waits until program is killed

	initializer.InitConfig()
	initializer.InitLogs()
	initializer.ConfigureGracefulDeath()

	initializer.ConfigureNodes()
	initializer.ConfigurePow()
	initializer.ConfigureTransactionsSender()

	initializer.InitTipsReceiver()

	waitGroup.Wait()
}
