package pow

import (
	"strings"
	"time"

	"github.com/iotaledger/giota"
	remotePiDiverWrapper "gitlab.com/brunoamancio/remotePoW/client"
	localPiDiverWrapper "gitlab.com/brunoamancio/remotePoW/server/powEngine/dcurlpidiver"
	"gitlab.com/brunoamancio/spartaSpammer/main/logs"
)

type TxPendingPow struct {
	Bundle                         giota.Bundle
	TransactionToApprove           *giota.GetTransactionsToApproveResponse
	TxPowCalculatedResponseChannel chan []giota.Transaction
}

type TrytesPendingPow struct {
	Trytes             string
	MinWeightMagniture int
	PowDoneChannel     chan string
}

var powServerAddresses []string
var powServerChannels map[string](chan *TrytesPendingPow)
var doLocalPow bool
var minWeightMagniture = 14

var TxsPendingPowChannel chan *TxPendingPow

func Configure(remotePowServerAddresses []string, mwm int) {
	TxsPendingPowChannel = make(chan *TxPendingPow)
	powServerAddresses = remotePowServerAddresses
	minWeightMagniture = mwm

	setPowServerChannels()

	go processPendingPow()
}

func setPowServerChannels() {
	powServerChannels = make(map[string](chan *TrytesPendingPow))

	for _, powServerAddress := range powServerAddresses {
		powServerChannels[powServerAddress] = make(chan *TrytesPendingPow, 10)

		go watchTrytesPendingPowChannel(powServerAddress)
	}

	if len(powServerChannels) == 0 {
		logs.Log.Fatal("Could not calculate remote pow. Cause: No pow server channel found.")
	}

	if len(powServerChannels) != len(powServerAddresses) {
		logs.Log.Fatal("Could not calculate remote pow. Cause: Amount of pow servers are not the same as pow server channels")
	}
}

func watchTrytesPendingPowChannel(powServerAddress string) {
	powFunction := getPowFunction(powServerAddress)

	powInLastPeriod := 0
	go poWServerReport(powServerAddress, &powInLastPeriod)

	for trytesPendingPow := range powServerChannels[powServerAddress] {
		logs.Log.Debug("Starting PoW calculation")
		resultTrytes, err := powFunction(powServerAddress, trytesPendingPow.Trytes, trytesPendingPow.MinWeightMagniture)

		if err != nil {
			logs.Log.Errorf("Could not calculate remote pow. Cause: ", err.Error())
			return
		}

		if len(resultTrytes) == 0 {
			logs.Log.Error("Could not calculate remote pow. Cause: No trytes were delivered")
			return
		}

		logs.Log.Debugf("PoW calculation from '%s' finished", powServerAddress)
		trytesPendingPow.PowDoneChannel <- resultTrytes
		powInLastPeriod++
	}
}

func poWServerReport(nodeAddress string, powInLastPeriod *int) {
	periodSeconds := 30
	minuteTicker := time.NewTicker(time.Duration(periodSeconds) * time.Second)

	for range minuteTicker.C {
		tps := float64(*powInLastPeriod) / float64(periodSeconds)
		logs.Log.Infof("Last 30 secs PoWs from '%s': %v - Average/s: %.1f", nodeAddress, *powInLastPeriod, tps)
		*powInLastPeriod = 0
	}
}

func processPendingPow() {
	for txPendingPow := range TxsPendingPowChannel {
		logs.Log.Debug("Processing TXPendingPoW")
		go prepareAndCalculatePow(txPendingPow)
	}
}

func prepareAndCalculatePow(txPendingPow *TxPendingPow) {
	bundle := txPendingPow.Bundle
	transactionsToApprove := txPendingPow.TransactionToApprove

	transactions := []giota.Transaction(bundle)

	trunkTransaction := transactionsToApprove.TrunkTransaction
	branchTransaction := transactionsToApprove.BranchTransaction

	var previousTransactionHash giota.Trytes

	var transactionsWithPow []giota.Transaction

	for currentTransactionIndex := len(transactions) - 1; currentTransactionIndex >= 0; currentTransactionIndex-- {
		transaction := refillTransaction(transactions, currentTransactionIndex, trunkTransaction, branchTransaction, previousTransactionHash)

		powDoneChannel := make(chan string, 1)

		go doPow(transaction.Trytes(), minWeightMagniture, powDoneChannel)

		// waits until pow is calculated by a worker
		resultTrytes := <-powDoneChannel
		nonceTrinaryOffsetInTrytes := giota.NonceTrinaryOffset / 3
		transaction.Nonce = giota.Trytes(resultTrytes[nonceTrinaryOffsetInTrytes:])
		//logs.Log.Debug("Nonce trytes: " + transaction.Nonce)

		previousTransactionHash = transaction.Hash()
		transactionsWithPow = append(transactionsWithPow, transaction)
	}

	logs.Log.Debug("Pow calculation finished.")
	txPendingPow.TxPowCalculatedResponseChannel <- transactionsWithPow
}

func refillTransaction(allTransactions []giota.Transaction, currentTransactionIndex int, trunkTransaction giota.Trytes, branchTransaction giota.Trytes, previousTransactionHash giota.Trytes) giota.Transaction {

	currentTransaction := allTransactions[currentTransactionIndex]

	switch {
	case currentTransactionIndex == len(allTransactions)-1:
		currentTransaction.TrunkTransaction = trunkTransaction
		currentTransaction.BranchTransaction = branchTransaction
	default:
		currentTransaction.TrunkTransaction = previousTransactionHash
		currentTransaction.BranchTransaction = trunkTransaction
	}

	timestamp := giota.Int2Trits(time.Now().UnixNano()/1000000, giota.TimestampTrinarySize).Trytes()
	currentTransaction.AttachmentTimestamp = timestamp
	currentTransaction.AttachmentTimestampLowerBound = giota.Int2Trits(0, giota.TimestampTrinarySize).Trytes()
	currentTransaction.AttachmentTimestampUpperBound = "MMMMMMMMM" // (3^27-1)/2

	return currentTransaction
}

var currentPowServerIndex = 0

func doPow(trytes giota.Trytes, minWeightMagniture int, powDoneChannel chan string) {
	if currentPowServerIndex >= len(powServerAddresses) {
		currentPowServerIndex = 0
	}

	trytesPendingPow := new(TrytesPendingPow)
	trytesPendingPow.Trytes = string(trytes)
	trytesPendingPow.MinWeightMagniture = minWeightMagniture
	trytesPendingPow.PowDoneChannel = powDoneChannel

	powServerAddress := powServerAddresses[currentPowServerIndex]
	currentPowServerIndex++

	logs.Log.Debugf("Delegating PoW to '%s'", powServerAddress)
	powServerChannels[powServerAddress] <- trytesPendingPow
}

func getPowFunction(powServerAddress string) func(powServerAddress string, stringTrytes string, minWeightMagniture int) (string, error) {

	doLocalPow := getDoLocalPow(powServerAddress)
	if doLocalPow {
		localPiDiverWrapper.InitializePowEngine()
		return localPiDiverWrapper.DoPoW
	}

	return remotePiDiverWrapper.DoRemotePoW
}

func getDoLocalPow(powServerAddress string) bool {
	doLocalPow = powServerAddress == "" || strings.Contains(powServerAddress, "localhost") || strings.Contains(powServerAddress, "127.0.0.1")
	return doLocalPow
}

func Dispose() {
	for powServerAddress, channel := range powServerChannels {
		doLocalPow := getDoLocalPow(powServerAddress)
		if doLocalPow {
			localPiDiverWrapper.Dispose()
		}
		close(channel)
	}
}
