package initializer

import (
	"os"
	"os/signal"

	"../config"
	"../logs"
	"../nodes"
	"../pow"
	"../tipsreceiver"
	"../transactionssender"
)

func InitConfig() {
	config.Init()
}

func InitLogs() {
	logLevel := config.Loader.GetString("logs.level")
	logs.Init(logLevel)
}

func ConfigureGracefulDeath() {
	go startDeathSignalWatcher()
}

func startDeathSignalWatcher() {
	ch := make(chan os.Signal, 5)
	signal.Notify(ch, os.Interrupt)

	logs.Log.Debug("Graceful death configured")

	<-ch // waits until receives something to unblock

	logs.Log.Info("FPGA POW is shutting down. Please wait...")
	finalizeGracefulDeath()
}

func finalizeGracefulDeath() {
	dispose()

	logs.Log.Info("*Dies in peace*")
	os.Exit(0)
}

func ConfigurePow() {
	powServerAddresses := config.Loader.GetStringSlice("workers.powServers")
	minWeightMagnitude := config.Loader.GetInt("transactions.minWeightMagnitude")

	pow.Configure(powServerAddresses, minWeightMagnitude)
}

func ConfigureNodes() {
	nodesSlice := config.Loader.GetStringSlice("nodes.addresses")
	blackListedNodesSlice := config.Loader.GetStringSlice("nodes.blackListedAddresses")
	nodes.LoadNodeAPIs(nodesSlice, blackListedNodesSlice)
}

func InitTipsReceiver() {
	maxDepth := config.Loader.GetInt("tips.maxDepth")
	tipReceivers := config.Loader.GetInt("workers.tipReceivers")

	tipsreceiver.Init(maxDepth, tipReceivers)
}

func ConfigureTransactionsSender() {
	recipientAddress := config.Loader.GetString("transactions.recipientAddress")
	tag := config.Loader.GetString("transactions.tag")
	message := config.Loader.GetString("transactions.message")
	campaign := config.Loader.GetString("transactions.campaign")

	transactionssender.Configure(recipientAddress, tag, message, campaign)
}

func dispose() {
	tipsreceiver.Dispose()
	transactionssender.Dispose()
}
