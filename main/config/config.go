package config

import (
	"os"

	"../logs"

	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// Loader allows retrieving infos about configuration
var Loader *viper.Viper

// Init initializes critical structures related to configuration
func Init() {
	Loader = viper.New()
	configPath := loadConfigFlags()
	loadConfigFile(configPath)
}

func loadConfigFlags() *string {
	flag.StringP("logs.level", "l", "INFO", "Minimum level to log")

	flag.IntP("tips.maxDepth", "d", 3, "Maximum depth to search for tips")

	flag.IntP("transactions.minWeightMagnitude", "w", 14, "Current minWeightMagnitude of the network")
	flag.StringP("transactions.recipientAddress", "a", "THIS9IS9SPARTA9999999999999999999THIS9IS9SPARTA99999999999999999999THIS9IS9SPARTA", "Address which receives spam transactions")
	flag.StringP("transactions.tag", "t", "99THIS99999IS99999SPARTA", "Tag of spam transactions")
	flag.StringP("transactions.message", "m", "THIS IS SPARTA!", "Message of spam transactions")
	flag.StringP("transactions.campaign", "g", "Tipsel research", "Added to message of spam transactions")

	flag.StringSliceP("nodes.addresses", "n", nil, "Nodes used to requests tips and to broadcast transactions")
	flag.StringSliceP("nodes.blackListedAddresses", "b", nil, "Nodes which will NOT be used by the spammer (Useful when loading lists of nodes via json)")

	flag.IntP("workers.tipReceivers", "r", 12, "Amount of workers which will request tips from nodes")

	flag.StringSliceP("workers.powServers", "p", nil, "Remote pow servers addresses")

	Loader.BindPFlags(flag.CommandLine)

	var configPath = flag.StringP("config", "c", "sparta.json", "Config file path")
	flag.Parse()
	return configPath
}

func loadConfigFile(configPath *string) {
	if len(*configPath) > 0 {
		_, err := os.Stat(*configPath)
		if !flag.CommandLine.Changed("config") && os.IsNotExist(err) {
			// Standard config file not found => skip
			logs.Log.Info("Standard config file not found. Loading default settings.")
		} else {
			logs.Log.Infof("Loading config from: %s", *configPath)
			Loader.SetConfigFile(*configPath)
			err := Loader.ReadInConfig()
			if err != nil {
				logs.Log.Fatalf("Config could not be loaded from: %s (%s)", *configPath, err)
			}
		}
	}
}
