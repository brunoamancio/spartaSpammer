package nodes

import (
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	"../logs"
	"../utils"
	"mvdan.cc/xurls"

	"github.com/iotaledger/giota"
)

var ApiNodes []*giota.API
var ApiNodesStrings []string

var waitGroup = &sync.WaitGroup{}

var blackList []string

func LoadNodeAPIs(nodes []string, blackListedNodes []string) {
	ApiNodesStrings = utils.ReadNodeList(nodes)
	setBlackList(blackListedNodes)

	for _, node := range ApiNodesStrings {
		if isNodeBlacklisted(node) {
			continue
		}

		logs.Log.Debugf("Initializing IOTA API for '%s'", node)

		waitGroup.Add(1)
		go addIfHealthy(node)
	}
	waitGroup.Wait()
	logs.Log.Debugf("Successfully added %v/%v nodes", len(ApiNodes), len(nodes))
}

func setBlackList(blackListedNodes []string) {
	if blackListedNodes == nil || len(blackListedNodes) == 0 {
		blackList = []string{}
	} else {
		blackList = blackListedNodes
	}
}

func addIfHealthy(node string) {

	apiNodeForHealthCheck := giota.NewAPI(node, &http.Client{Timeout: 5 * time.Second})
	isHealthy, err := checkIfHealthy(node, apiNodeForHealthCheck)

	if isHealthy {
		apiNode := giota.NewAPI(node, &http.Client{Timeout: 30 * time.Second})
		ApiNodes = append(ApiNodes, apiNode)
		logs.Log.Debugf("IOTA API for '%s' initialized", node)
	} else {

		var causeText string
		if err != nil {
			causeText = err.Error()
		} else {
			causeText = "Node is not healthy"
		}

		logs.Log.Warningf("Could not initialize IOTA API for '%s'. Cause: %s", node, causeText)
	}

	waitGroup.Add(-1)
}

func checkIfHealthy(node string, apiNode *giota.API) (bool, error) {
	nodeInfoResponse, err := apiNode.GetNodeInfo()

	if err != nil {
		return false, err
	}

	const acceptableMilestonesDelay = 3
	isSynced := nodeInfoResponse.LatestMilestoneIndex-nodeInfoResponse.LatestSolidSubtangleMilestoneIndex <= acceptableMilestonesDelay
	isHercules := strings.Contains(nodeInfoResponse.AppName, "Hercules")
	logs.Log.Infof("Node '%s' is synced and is running %s %s", node, nodeInfoResponse.AppName, nodeInfoResponse.AppVersion)
	return isSynced || isHercules, nil
}

func GetRandomApiNode() *giota.API {
	if len(ApiNodes) == 0 {
		logs.Log.Fatal("No node has been added")
	}
	randomNumber := utils.Random(0, len(ApiNodes))
	return ApiNodes[randomNumber]
}

func GetNodeAddress(apiNode *giota.API) string {
	apiNodeAddress := fmt.Sprintf("%#v", apiNode)
	apiNodeAddress = xurls.Strict().FindString(apiNodeAddress)
	return apiNodeAddress
}

func HandleNodeResponse(errorMessage string) (bool, string) {
	goodNodeResponse := true
	modifiedResponse := ""

	sentTooManyMessages := strings.Contains(errorMessage, "http status 429") || strings.Contains(errorMessage, "Too Many Requests")
	if sentTooManyMessages {
		// sleepTime := 15
		// modifiedResponse = "Reached node's maximum allowed requests per second. Sleeping for 15s"
		// time.Sleep(time.Second * (time.Duration)(sleepTime))
		goodNodeResponse = false
		modifiedResponse = "Reached node's maximum allowed requests per second."
	} else {
		isNotAvailable := strings.Contains(errorMessage, "is not available") || strings.Contains(errorMessage, "timeout") || strings.Contains(errorMessage, "Timeout") || strings.Contains(errorMessage, "no such host")
		if isNotAvailable {
			goodNodeResponse = false
		}
		hasCommunicationIssues := strings.Contains(errorMessage, "invalid character") || strings.Contains(errorMessage, "no route to host") || strings.Contains(errorMessage, "connection refused") || strings.Contains(errorMessage, "connection reset by peer")
		if hasCommunicationIssues {
			goodNodeResponse = false
		}

		hasCertificateIssues := strings.Contains(errorMessage, "certificate is valid for")
		if hasCertificateIssues {
			goodNodeResponse = false
			modifiedResponse = "Node has issue with its ssl certificate"
		}
	}

	return goodNodeResponse, modifiedResponse
}

func isNodeBlacklisted(nodeAddress string) bool {
	for _, blacklistedAddress := range blackList {
		if nodeAddress == blacklistedAddress {
			return true
		}
	}

	return false
}
